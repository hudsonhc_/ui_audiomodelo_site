$(function() {

    $(document).ready(function() {
        $(".sub-menu").addClass("dropdown-menu")
    }),
    $(".dropdown .sub-menu").addClass("dropdown-menu"), $(".dropdown").mouseover(function() {
        $(this).addClass("open")
    }),
    $(".dropdown ").mouseout(function() {
        $(this).parent().removeClass("open")
    }),
    $(".dropdown").mouseout(function() {
        $(this).removeClass("open")
    }),
    $(".ancoraTopo").hide(), $(window).bind("scroll", function() {
        if ($(window).width() > 768) {
            var o = $(window).scrollTop();
            o > 41 ? ($(".topoContato").slideUp(), $(".ancoraTopo").slideDown(), $(".topo").mouseenter(function() {
                $(".topoContato").slideDown()
            })) : ($(".topoContato").slideDown(), $(".ancoraTopo").slideUp())
        }
    }),

    $(document).ready(function() {

        $("#carrosselDestaque").find(".item").length > 1 ? $("#carrosselDestaque").owlCarousel({
            items: 1,
            dots: !0,
            loop: true,
            lazyLoad: !0,
            mouseDrag: !0,
            touchDrag: !0,
            autoplayTimeout: 5e3,
            autoplayHoverPause: !0,
            animateOut: "fadeOut",
            smartSpeed: 450,
            center: !0
        }) : $("#carrosselDestaque").owlCarousel({
            items: 1,
            dots: !0,
            loop: !1,
            lazyLoad: !0,
            mouseDrag: !0,
            touchDrag: !0,
            autoplayTimeout: 5e3,
            autoplayHoverPause: !0,
            animateOut: "fadeOut",
            smartSpeed: 450,
            center: !0
        })
        //BOTÕES DO CARROSSEL DE PARCERIA
        var carrossel_destaque = $("#carrosselDestaque").data('owlCarousel');
        $('#btnCarrosselLeft').click(function(){
            //$('.youtube-video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
            carrossel_destaque.prev();
        });
        $('#btnCarrosselRight').click(function(){
            //$('.youtube-video')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
            carrossel_destaque.next();
        });
    }),

         $("#playVideo").click(function(o) {
        var e = document.getElementById("video");
        e.currentTime = 0, $("#video")[0].play(), $(this).hide()
    }), $("#video").click(function(o) {
        $("#playVideo").show(), $("#video")[0].pause()
    }), $(".conteudo .collapse").click(function(o) {
        $(this).children(".fa-plus").hide(), 0 == $(this).hasClass("collapsed") ? $(this).children(".fa-plus").show() : 1 == $(this).hasClass("collapsed") && $(this).children(".fa-plus").hide()
    }), $(document).ready(function() {
        $("#carrosselAgencias").owlCarousel({
            items: 5,
            loop: !0,
            lazyLoad: !0,
            autoplay: !0,
            autoplayTimeout: 2e3,
            autoplayHoverPause: !0,
            smartSpeed: 450,
            responsiveClass: !0,
            responsive: {
                320: {
                    items: 1
                },
                600: {
                    items: 2
                },
                991: {
                    items: 2
                },
                1024: {
                    items: 5
                },
                1440: {
                    items: 5
                }
            }
        })
    }), $(document).ready(function() {
        $("#carrosselClientesTopo").find(".item").length > 1 ? $("#carrosselClientesTopo").owlCarousel({
            items: 6,
            dots: !0,
            loop: true,
            //lazyLoad: !0,
            mouseDrag: true,
            //touchDrag: !1,
            autoplay: true,
            autoplayTimeout: 1500,
            //autoplaySpeed: 100,
            //autoplayHoverPause: false,
            //smartSpeed: 200,

            //CARROSSEL RESPONSIVO
            responsiveClass:true,
            responsive:{
                320:{
                    items:2
                },
                600:{
                    items:3
                },

                991:{
                    items:4
                },
                1024:{
                    items:6
                },
                1440:{
                    items:6
                },
                1920: {
                    items:6
                },

            }
        }) : $("#carrosselClientesTopo").owlCarousel({
            items: 6,
            dots: !0,
            loop: !1,
            lazyLoad: !0,
            mouseDrag: true,
            touchDrag: !1,
            autoplay: true,
            autoplayTimeout: 0,
            autoplayHoverPause: !0,
            animateOut: "fadeOut",
            smartSpeed: 450,

            //CARROSSEL RESPONSIVO
            responsiveClass:true,
            responsive:{
                320:{
                    items:2
                },
                600:{
                    items:3
                },

                991:{
                    items:4
                },
                1024:{
                    items:6
                },
                1440:{
                    items:6
                },
                1920: {
                    items:6
                },

            }
         })
    }),
    $(document).ready(function() {
        $("#carrosselClientes").find(".item").length > 1 ? $("#carrosselClientes").owlCarousel({
            items: 4,
            dots: !0,
            loop: true,
            lazyLoad: !0,
            mouseDrag: true,
            touchDrag: !1,
            autoplay: true,
            autoplayTimeout: 2e3,
            autoplayHoverPause: !0,
            animateOut: "fadeOut",
            smartSpeed: 450,

            //CARROSSEL RESPONSIVO
            responsiveClass:true,
            responsive:{
                320:{
                    items:1
                },
                600:{
                    items:2
                },

                991:{
                    items:3
                },
                1024:{
                    items:4
                },
                1440:{
                    items:4
                },
                1920: {
                    items:4
                },

            }
        }) : $("#carrosselClientes").owlCarousel({
            items: 4,
            dots: !0,
            loop: !1,
            lazyLoad: !0,
            mouseDrag: true,
            touchDrag: !1,
            autoplay: true,
            autoplayTimeout: 2e3,
            autoplayHoverPause: !0,
            animateOut: "fadeOut",
            smartSpeed: 450,

            //CARROSSEL RESPONSIVO
            responsiveClass:true,
            responsive:{
                320:{
                    items:1
                },
                600:{
                    items:2
                },

                991:{
                    items:3
                },
                1024:{
                    items:4
                },
                1440:{
                    items:4
                },
                1920: {
                    items:4
                },

            }
         })
         //BOTÕES DO CARROSSEL DE PARCERIA
         var carrossel_clientesEsquerda = $("#carrosselClientes").data('owlCarousel');
         $('#btncarrosselClientesLeft').click(function(){
             carrossel_clientesEsquerda.next();
         });
         var carrossel_clientesDireita = $("#carrosselClientes").data('owlCarousel');
         $('#btncarrosselClientesRight').click(function(){
             carrossel_clientesDireita.prev();
         });
    }),$(document).ready(function() {
        $("#carrosselDepoimentos").find(".item").length > 1 ? $("#carrosselDepoimentos").owlCarousel({
            items: 3,
            dots: !0,
            loop: true,
            lazyLoad: !0,
            mouseDrag: true,
            touchDrag: true,
            autoplay: true,
            autoplayTimeout: 2e3,
            autoplayHoverPause: !0,
            animateOut: "fadeOut",
            smartSpeed: 450,

            //CARROSSEL RESPONSIVO
            responsiveClass:true,
            responsive:{
                320:{
                    items:1
                },
                600:{
                    items:2
                },

                991:{
                    items:2
                },
                1024:{
                    items:3
                },
                1440:{
                    items:3
                },

            }
        }) : $("#carrosselDepoimentos").owlCarousel({
            items: 1,
            dots: !0,
            loop: !1,
            lazyLoad: !0,
            mouseDrag: false,
            touchDrag: !1,
            autoplay: true,
            autoplayTimeout: 2e3,
            autoplayHoverPause: !0,
            animateOut: "fadeOut",
            smartSpeed: 450,

            //CARROSSEL RESPONSIVO
            responsiveClass:true,
            responsive:{
                320:{
                    items:1
                },
                600:{
                    items:2
                },

                991:{
                    items:2
                },
                1024:{
                    items:3
                },
                1440:{
                    items:3
                },

            }
         })
         var carrossel_depoimentosLeft = $("#carrosselDepoimentos").data('owlCarousel');
         $('#btncarrosselDepoimentosLeft').click(function(){
             carrossel_depoimentosLeft.next();
         });
         var carrossel_depoimentosRight = $("#carrosselDepoimentos").data('owlCarousel');
         $('#btncarrosselDepoimentosRight').click(function(){
             carrossel_depoimentosRight.prev();
         });
    }), $("a#fancy").fancybox({
        titleShow: !1,
        openEffect: "elastic",
        closeEffect: "elastic",
        closeBtn: !0,
        arrows: !0,
        nextClick: !0
    }), $(".linkPaginas").click(function(o) {
        if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
            var e = $(this.hash);
            if (e = e.length ? e : $("[name=" + this.hash.slice(1) + "]"), e.length) return $("html,body").animate({
                scrollTop: e.offset().top
            }, 1e3), !1
        }
    }), $(".ancoraTopo").click(function(o) {
        if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
            var e = $(this.hash);
            if (e = e.length ? e : $("[name=" + this.hash.slice(1) + "]"), e.length) return $("html,body").animate({
                scrollTop: e.offset().top
            }, 1e3), !1
        }
    }),

    $(window).load(function() {

       $("span.number").each(function() {
            var o = $(this);
            $({
                Counter: 0
            }).animate({
                Counter: o.attr("data-stop")
            }, {
                duration: 5e3,
                easing: "swing",
                step: function(e) {
                    o.text(Math.ceil(e))
                }
            })
        });
            $({
                Counter: 0
            }).animate({
                Counter: o.attr("data-stop")
            }, {
                duration: 5e3,
                easing: "swing",
                step: function(e) {
                    o.text(Math.ceil(e))
                }
            })

    })


    /**********************************************************************
        SCRIPTS ANIMAÇÃO DAS ANCORAS
    **********************************************************************/
        $('li.ancora a').click(function() {
            $( '.topo .areaMenu .navbar-toggle' ).toggleClass( "mostrarX" );
            $(".navbar-collapse").removeClass('in');
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
           
        });

        $('.ancora').click(function() {
            $(".navbar-collapse").removeClass('in');
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
            
        });

        $('.itemSolicitarOcamento').click(function() {
            event.preventDefault();
            $("#formOrcamento").fadeIn();
            $("body").addClass("travarScroll");
        });

        $('.abrirFormularioDeOrcamento').click(function() {
            event.preventDefault();
            $("#formOrcamento").fadeIn();
            $("body").addClass("travarScroll");
        });
        
        $('.solicitarProposta').click(function() {
            $("#formOrcamento").fadeIn();
            $("body").addClass("travarScroll");
        });

        $('#fecharOrcamento').click(function() {
            $("#formOrcamento").fadeOut();
            $("body").removeClass("travarScroll");
        });

        $(document).keyup(function(e) {
            if (e.keyCode == 27) {
                $("#formOrcamento").fadeOut();
                $(".formularioArquivos").fadeOut();
                $(".formularioEntreParaOTime").fadeOut();
                $("body").removeClass("travarScroll");
            }
        });

        //SCRIPT BOTAO PARA ABRIR FORMULÁRIO DE ENVIO DE ARQUIVOS
        $('.abrirModalEntreParaOTime').click(function() {
            event.preventDefault();
            $(".formularioEntreParaOTime").fadeIn();
            $("body").addClass("travarScroll");
        });

        $('.fecharEntreParaOTime').click(function() {
            $(".formularioEntreParaOTime").fadeOut();
            $("body").removeClass("travarScroll");
        });


        $('.pg .areaServicos .postServicos .abrirPostServicos').click(function(e){
         $( this ).toggleClass( "iconePlus" );
        });
    
        $('.topo .areaMenu .navbar-toggle').click(function(e){
         $( this ).toggleClass( "mostrarX" );
        });
        
       // $('.pg .areaServicos .postServicos .abrirPostServicos').click(function(e){
          //  $(this).next().children('a').trigger();
       // });
       setTimeout(function(){  
            $(".topo .areaMenu .navbar-nav > li").css({
                "height":"auto",
                "overflow":"initial",
            });
        }, 1000);
        $(".item button").click(function(){
        let idVaga = $(this).attr("data-id");
        $( ".vagasDisponveis .areaDescricaoVagas").hide();
        $( ".vagasDisponveis " + "#"+idVaga).show();
        $(".vagasDisponveis .opcoesVagas .item button").removeClass("categoriaAtiva");        
        $(this).addClass("categoriaAtiva");    
    });
    
    //CARROSSEL DE DESTAQUE
    $("#carrosselDeVagas").owlCarousel({
        items : 5,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,         
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        smartSpeed: 450,
        margin: 0,

        //CARROSSEL RESPONSIVO
        responsiveClass:true,               
         responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },

            991:{
                items:3
            },
            1024:{
                items:5
            },
            1440:{
                items:5
            },
                                    
        }                              
        
    });

});
