<?php
/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */
  /*********************************************
              //FUNÇÕES//
  **********************************************/
        if ( ! class_exists( 'Redux' ) ) {
            return;
        }
        // This is your option name where all the Redux data is stored.
        $opt_name = "configuracao";
        // This line is only for altering the demo. Can be easily removed.
        //$opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );
        /*
         *
         * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
         *
         */
        $sampleHTML = '';
        if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
            Redux_Functions::initWpFilesystem();
            global $wp_filesystem;
            $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
        }
        // Background Patterns Reader
        $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
        $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
        $sample_patterns      = array();
        if ( is_dir( $sample_patterns_path ) ) {
            if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
                $sample_patterns = array();
                while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {
                    if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                        $name              = explode( '.', $sample_patterns_file );
                        $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                        $sample_patterns[] = array(
                            'alt' => $name,
                            'img' => $sample_patterns_url . $sample_patterns_file
                        );
                    }
                }
            }
        }
        $theme = wp_get_theme(); // For use with some settings. Not necessary.
        $args = array(
            // TYPICAL -> Change these values as you need/desire
            'opt_name'             => $opt_name,
            // This is where your data is stored in the database and also becomes your global variable name.
            'display_name'         => $theme->get( 'Name' ),
            // Name that appears at the top of your panel
            'display_version'      => $theme->get( 'Version' ),
            // Version that appears at the top of your panel
            'menu_type'            => 'menu',
            //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
            'allow_sub_menu'       => true,
            // Show the sections below the admin menu item or not
            'menu_title'           => __( 'Editar informações do site', 'redux-framework-demo' ),
            'page_title'           => __( 'Editar informações do site', 'redux-framework-demo' ),
            // You will need to generate a Google API key to use this feature.
            // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
            'google_api_key'       => '',
            // Set it you want google fonts to update weekly. A google_api_key value is required.
            'google_update_weekly' => false,
            // Must be defined to add google fonts to the typography module
            'async_typography'     => true,
            // Use a asynchronous font on the front end or font string
            //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
            'admin_bar'            => true,
            // Show the panel pages on the admin bar
            'admin_bar_icon'       => 'dashicons-portfolio',
            // Choose an icon for the admin bar menu
            'admin_bar_priority'   => 50,
            // Choose an priority for the admin bar menu
            'global_variable'      => '',
            // Set a different name for your global variable other than the opt_name
            'dev_mode'             => false,
            // Show the time the page took to load, etc
            'update_notice'        => true,
            // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
            'customizer'           => true,
            // Enable basic customizer support
            //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
            //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field
            // OPTIONAL -> Give you extra features
            'page_priority'        => null,
            // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
            'page_parent'          => 'themes.php',
            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
            'page_permissions'     => 'manage_options',
            // Permissions needed to access the options panel.
            'menu_icon'            => '',
            // Specify a custom URL to an icon
            'last_tab'             => '',
            // Force your panel to always open to a specific tab (by id)
            'page_icon'            => 'icon-themes',
            // Icon displayed in the admin panel next to your menu_title
            'page_slug'            => '',
            // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
            'save_defaults'        => true,
            // On load save the defaults to DB before user clicks save or not
            'default_show'         => false,
            // If true, shows the default value next to each field that is not the default value.
            'default_mark'         => '',
            // What to print by the field's title if the value shown is default. Suggested: *
            'show_import_export'   => true,
            // Shows the Import/Export panel when not used as a field.

            // CAREFUL -> These options are for advanced use only
            'transient_time'       => 60 * MINUTE_IN_SECONDS,
            'output'               => true,
            // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
            'output_tag'           => true,
            // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
            // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
            // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
            'database'             => '',
            // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
            'use_cdn'              => true,
            // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.
            // HINTS
            'hints'                => array(
                'icon'          => 'el el-question-sign',
                'icon_position' => 'right',
                'icon_color'    => 'lightgray',
                'icon_size'     => 'normal',
                'tip_style'     => array(
                    'color'   => 'red',
                    'shadow'  => true,
                    'rounded' => false,
                    'style'   => '',
                ),
                'tip_position'  => array(
                    'my' => 'top left',
                    'at' => 'bottom right',
                ),
                'tip_effect'    => array(
                    'show' => array(
                        'effect'   => 'slide',
                        'duration' => '500',
                        'event'    => 'mouseover',
                    ),
                    'hide' => array(
                        'effect'   => 'slide',
                        'duration' => '500',
                        'event'    => 'click mouseleave',
                    ),
                ),
            )
        );
        // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
        $args['admin_bar_links'][] = array(
            'id'    => 'redux-docs',
            'href'  => 'http://docs.reduxframework.com/',
            'title' => __( 'Documentation', 'redux-framework-demo' ),
        );
        $args['admin_bar_links'][] = array(
            //'id'    => 'redux-support',
            'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
            'title' => __( 'Support', 'redux-framework-demo' ),
        );
        $args['admin_bar_links'][] = array(
            'id'    => 'redux-extensions',
            'href'  => 'reduxframework.com/extensions',
            'title' => __( 'Extensions', 'redux-framework-demo' ),
        );
        // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
        $args['share_icons'][] = array(
            'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
            'title' => 'Visit us on GitHub',
            'icon'  => 'el el-github'
            //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
        );
        $args['share_icons'][] = array(
            'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
            'title' => 'Like us on Facebook',
            'icon'  => 'el el-facebook'
        );
        $args['share_icons'][] = array(
            'url'   => 'http://twitter.com/reduxframework',
            'title' => 'Follow us on Twitter',
            'icon'  => 'el el-twitter'
        );
        $args['share_icons'][] = array(
            'url'   => 'http://www.linkedin.com/company/redux-framework',
            'title' => 'Find us on LinkedIn',
            'icon'  => 'el el-linkedin'
        );

        // Panel Intro text -> before the form
        if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
            if ( ! empty( $args['global_variable'] ) ) {
                $v = $args['global_variable'];
            } else {
                $v = str_replace( '-', '_', $args['opt_name'] );
            }
            $args['intro_text'] = sprintf( __( '', 'redux-framework-demo' ) );
        } else {
            $args['intro_text'] = __( '', 'redux-framework-demo' );
        }
        // Add content after the form.
        $args['footer_text'] = __( '<p></p>', 'redux-framework-demo' );

        Redux::setArgs( $opt_name, $args );
        /*
         * ---> END ARGUMENTS
         */
        /*
         * ---> START HELP TABS
         */
        $tabs = array(
            array(
                'id'      => 'redux-help-tab-1',
                'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
                'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
            ),
            array(
                'id'      => 'redux-help-tab-2',
                'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
                'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
            )
        );
        Redux::setHelpTab( $opt_name, $tabs );
        // Set the help sidebar
        $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
        Redux::setHelpSidebar( $opt_name, $content );
        /*
         * <--- END HELP TABS
         */
        /*
         *
         * ---> START SECTIONS
         *
         */
    /*********************************************
              CAMPOS PERSONALIZADOS
    **********************************************/
        //SESSÃO PÁGINA INICIAL
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Configurações de conteúdo: Página inicial', 'redux-framework-demo' ),
            'id'               => 'paginas',
            'desc'             => __( 'Frases, textos e informações de cada sessão', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            )
        );
            //SESSÃO PÁGINA INICIAL - ÁREA TEXTO QUEM SOMOS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área texto quem somos', 'redux-framework-demo' ),
                    'id'               => 'paginas_inicial_quem_somos',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração quem somos', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_inicial_quem_somos_texto',
                            'type'     => 'editor',
                            'title'    => __( 'Texto quem somos', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_inicial_quem_somos_btn',
                            'type'     => 'text',
                            'title'    => __( 'Título do botão', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_inicial_quem_somos_btn_link',
                            'type'     => 'text',
                            'title'    => __( 'Link do botão', 'redux-framework-demo' ),
                            'desc'    => __( 'Ex:http://www.exemplo.com.br', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'    => 'opt_inicial_quem_somos_hidden',
                            'type'  => 'button_set',
                            'desc'  => 'Selecione se deseja mostrar essa área',
                            'title' => 'Área quem somos',
                            'options' => array(
                                'Esconder' => 'Esconder',

                                'Mostrar' => 'Mostrar',

                            ),

                        ),

                    )

                )

            );

            //SESSÃO PÁGINA INICIAL - COMO FUNCIONA
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área como funciona', 'redux-framework-demo' ),
                    'id'               => 'paginas_inicial_como_funciona',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração como funciona', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                         array(
                            'id'       => 'opt_inicial_como_funciona_btn',
                            'type'     => 'text',
                            'title'    => __( 'Título do botão', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_inicial_como_funciona_btn_link',
                            'type'     => 'text',
                            'title'    => __( 'Link do botão', 'redux-framework-demo' ),
                            'desc'    => __( 'Ex:http://www.exemplo.com.br', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'    => 'opt_inicial_como_funciona_hidden',
                            'type'  => 'button_set',
                            'desc'  => 'Selecione se deseja mostrar essa área',
                            'title' => 'Área como funciona',
                            'options' => array(
                                'Esconder' => 'Esconder',
                                'Mostrar' => 'Mostrar',
                            ),
                       ),
                    )
                )
            );

            //SESSÃO PÁGINA INICIAL - ÁREA DE ESTATISTICAS TOPO
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Sessão de estatisticas topo', 'redux-framework-demo' ),
                    'id'               => 'paginas_inicial_estatisticas',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração da sessão de estatísticas:', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'paginas_inicial_valor1',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da primeiro sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_texto_valor1',
                            'type'     => 'text',
                            'title'    => __( 'Texto da primeira sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_valor2',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da segunda sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_texto_valor2',
                            'type'     => 'text',
                            'title'    => __( 'Texto da segunda sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_valor3',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da terceira sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_texto_valor3',
                            'type'     => 'text',
                            'title'    => __( 'Texto da terceira sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_valor4',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da quarta sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_texto_valor4',
                            'type'     => 'text',
                            'title'    => __( 'Texto da quarta sessão:', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA INICIAL - ÁREA CLIENTES
            Redux::setSection( $opt_name, array(

                    'title'            => __( 'Área  logo clientes', 'redux-framework-demo' ),

                    'id'               => 'paginas_inicial_logo_clientes',

                    'subsection'       => true,

                    'desc'             => __( 'Configuração área clientes', 'redux-framework-demo' ),

                    'customizer_width' => '450px',

                    'fields'           => array(

                        array(

                            'id'       => 'paginas_inicial_logo_titulo',

                            'type'     => 'text',

                            'title'    => __( 'Título área clientes', 'redux-framework-demo' ),

                        ),

                        array(

                            'id'    => 'paginas_inicial_logo_hidden',

                            'type'  => 'button_set',

                            'desc'  => 'Selecione se deseja mostrar essa área',

                            'title' => 'Área clientes',

                            'options' => array(

                                'Esconder' => 'Esconder',

                                'Mostrar' => 'Mostrar',

                            ),

                        ),

                    )

                )

            );

            //SESSÃO PÁGINA INICIAL - ÁREA DEPOIMENTOS
            Redux::setSection( $opt_name, array(

                    'title'            => __( 'Área  depoimentos', 'redux-framework-demo' ),

                    'id'               => 'paginas_inicial_depoimentos',

                    'subsection'       => true,

                    'desc'             => __( 'Configuração área depoimentos', 'redux-framework-demo' ),

                    'customizer_width' => '450px',

                    'fields'           => array(

                        array(

                            'id'       => 'paginas_inicial_depoimentos_titulo',

                            'type'     => 'text',

                            'title'    => __( 'Título área depoimentos', 'redux-framework-demo' ),

                        ),



                        array(

                            'id'    => 'paginas_inicial_depoimentos_hidden',

                            'type'  => 'button_set',

                            'desc'  => 'Selecione se deseja mostrar essa área',

                            'title' => 'Área depoimentos',

                            'options' => array(

                                'Esconder' => 'Esconder',

                                'Mostrar' => 'Mostrar',

                            ),

                        ),

                    )

                )

            );

            //SESSÃO PÁGINA INICIAL - PORQUE COMNFIAR NA AUDIOTEXT
            Redux::setSection( $opt_name, array(

                    'title'            => __( 'Área valores audiotext', 'redux-framework-demo' ),

                    'id'               => 'paginas_inicial_valores_audiotext',

                    'subsection'       => true,

                    'desc'             => __( 'Configuração área valores audiotext', 'redux-framework-demo' ),

                    'customizer_width' => '450px',

                    'fields'           => array(

                        array(

                            'id'       => 'paginas_inicial_valores_audiotext_titulo',

                            'type'     => 'text',

                            'title'    => __( 'Título área valores', 'redux-framework-demo' ),

                        ),



                        array(

                            'id'    => 'paginas_inicial_valores_audiotext_hidden',

                            'type'  => 'button_set',

                            'desc'  => 'Selecione se deseja mostrar essa área',

                            'title' => 'Área valores',

                            'options' => array(

                                'Esconder' => 'Esconder',

                                'Mostrar' => 'Mostrar',

                            ),

                        ),

                    )

                )

            );

            //SESSÃO PÁGINA INICIAL - SERVIÇOS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área de serviços', 'redux-framework-demo' ),
                    'id'               => 'paginas_inicial_servicos_audiotext',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração área de serviços', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'paginas_inicial_servicos_audiotext_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título área serviços', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'    => 'paginas_inicial_servicos_audiotext_hidden',
                            'type'  => 'button_set',
                            'desc'  => 'Selecione se deseja mostrar essa área',

                            'title' => 'Área serviços',

                            'options' => array(

                                'Esconder' => 'Esconder',

                                'Mostrar' => 'Mostrar',

                            ),

                        ),

                    )

                )

            );

            //SESSÃO PÁGINA INICIAL - INFORMAÇÕES DE HORAS TRANSCRITAS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área de informações de horas transcritas...', 'redux-framework-demo' ),
                    'id'               => 'paginas_inicial_info_audiotext',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração área informações de horas transcritas', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'paginas_inicial_info_audiotext_titulo_1',
                            'type'     => 'text',
                            'title'    => __( 'Título 1', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_audiotext_valor_1',
                            'type'     => 'text',
                            'title'    => __( 'valor 1', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_audiotext_desc_1',
                            'type'     => 'textarea',
                            'title'    => __( 'descrição 1', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_audiotext_titulo_2',
                            'type'     => 'text',
                            'title'    => __( 'Título 2', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_audiotext_valor_2',
                            'type'     => 'text',
                            'title'    => __( 'valor 2', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_audiotext_desc_2',
                            'type'     => 'textarea',
                            'title'    => __( 'descrição 2', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_audiotext_titulo_3',
                            'type'     => 'text',
                            'title'    => __( 'Título 3', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_audiotext_valor_3',
                            'type'     => 'text',
                            'title'    => __( 'valor 3', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_audiotext_desc_3',
                            'type'     => 'textarea',
                            'title'    => __( 'descrição 3', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_audiotext_titulo_4',
                            'type'     => 'text',
                            'title'    => __( 'titulo 4', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_audiotext_valor_4',
                            'type'     => 'text',
                            'title'    => __( 'valor 4', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'    => 'paginas_inicial_info_audiotext_hidden',
                            'type'  => 'button_set',
                            'desc'  => 'Selecione se deseja mostrar essa área',
                            'title' => 'Área informações',
                            'options' => array(
                                'Esconder' => 'Esconder',
                                'Mostrar' => 'Mostrar',
                            ),
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA INICIAL - INFORMAÇÕES FLEXIBILIDADE
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área de informações de flexibilidade inteligente...', 'redux-framework-demo' ),
                    'id'               => 'paginas_inicial_info_flexibilidade_audiotext',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração área informações de flexibilidade inteligente', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'paginas_inicial_info_flexibilidade_audiotext_icone_1',
                            'type'     => 'text',
                            'title'    => __( 'ícone 1', 'redux-framework-demo' ),
                            'desc'    => __( 'ícones utilizados http://fontawesome.io/icons/', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_flexibilidade_audiotext_titulo_1',
                            'type'     => 'text',
                            'title'    => __( 'Título 1', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_flexibilidade_audiotext_desc_1',
                            'type'     => 'textarea',
                            'title'    => __( 'descrição 1', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_flexibilidade_audiotext_icone_2',
                            'type'     => 'text',
                            'title'    => __( 'ícone 2', 'redux-framework-demo' ),
                            'desc'    => __( 'ícones utilizados http://fontawesome.io/icons/', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_flexibilidade_audiotext_titulo_2',
                            'type'     => 'text',
                            'title'    => __( 'Título 2', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_flexibilidade_audiotext_desc_2',
                            'type'     => 'textarea',
                            'title'    => __( 'descrição 2', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_flexibilidade_audiotext_icone_3',
                            'type'     => 'text',
                            'title'    => __( 'ícone 3', 'redux-framework-demo' ),
                            'desc'    => __( 'ícones utilizados http://fontawesome.io/icons/', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_flexibilidade_audiotext_titulo_3',
                            'type'     => 'text',
                            'title'    => __( 'Título 3', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_inicial_info_flexibilidade_audiotext_desc_3',
                            'type'     => 'textarea',
                            'title'    => __( 'descrição 3', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'    => 'paginas_inicial_info_flexibilidade_audiotext_hidden',
                            'type'  => 'button_set',
                            'desc'  => 'Selecione se deseja mostrar essa área',
                            'title' => 'Área informações',
                            'options' => array(
                                'Esconder' => 'Esconder',
                                'Mostrar' => 'Mostrar',
                            ),
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA INICIAL - ÁREA TEXTO QUEM SOMOS FOOTER
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área texto quem somos rodapé', 'redux-framework-demo' ),
                    'id'               => 'paginas_inicial_quem_somos_rodape',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração quem somos rodapé', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_inicial_quem_somos_img_footer',
                            'type'     => 'media',
                            'title'    => __( 'Imagem do ilustrativa - parallax', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_inicial_quem_somos_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_inicial_quem_somos_texto_footer',

                            'type'     => 'editor',

                            'title'    => __( 'Texto quem somos', 'redux-framework-demo' ),

                        ),



                        array(

                            'id'       => 'opt_inicial_quem_somos_imagem_equipe',

                            'type'     => 'media',

                            'title'    => __( 'Imagem da equipe Audiotext', 'redux-framework-demo' ),

                        ),



                        array(

                            'id'       => 'opt_inicial_quem_somos_btn_footer',

                            'type'     => 'text',

                            'title'    => __( 'Título do botão', 'redux-framework-demo' ),

                        ),

                        array(

                            'id'       => 'opt_inicial_quem_somos_btn_link_footer',

                            'type'     => 'text',

                            'title'    => __( 'Link do botão', 'redux-framework-demo' ),

                            'desc'    => __( 'Ex:http://www.exemplo.com.br', 'redux-framework-demo' ),

                        ),

                        array(

                            'id'    => 'opt_inicial_quem_somos_footer_hidden',

                            'type'  => 'button_set',

                            'desc'  => 'Selecione se deseja mostrar essa área',

                            'title' => 'Área quem somos',

                            'options' => array(

                                'Esconder' => 'Esconder',

                                'Mostrar' => 'Mostrar',

                            ),

                        ),

                    )

                )

            );

            //SESSÃO PÁGINA INICIAL - ÁREA SEJA UM TEXTER
            Redux::setSection( $opt_name, array(

                    'title'            => __( 'Área seja um texter', 'redux-framework-demo' ),

                    'id'               => 'paginas_inicial_seja_um_texter',

                    'subsection'       => true,

                    'desc'             => __( 'Configuração seja um texter', 'redux-framework-demo' ),

                    'customizer_width' => '450px',

                    'fields'           => array(

                        array(

                            'id'       => 'opt_inicial_seja_um_texter',

                            'type'     => 'text',

                            'title'     => '´Título',

                        ),

                        array(

                            'id'       => 'opt_inicial_seja_um_texter_btn',

                            'type'     => 'text',

                            'title'     => 'Título do botão',

                        ),

                       array(

                            'id'       => 'opt_inicial_seja_um_texter_btn_link',

                            'type'     => 'text',

                            'title'     => 'Link do botão',

                        ),

                        array(

                            'id'    => 'paginas_inicial_seja_um_texter_hidden',

                            'type'  => 'button_set',

                            'desc'  => 'Selecione se deseja mostrar essa área',

                            'title' => 'Área seja um texter',

                            'options' => array(

                                'Esconder' => 'Esconder',

                                'Mostrar' => 'Mostrar',

                            ),

                        ),

                    )

                )

            );

        //SESSÃO PÁGINA TRANSCRIÇÃO DE ÁUDIO EM TEXTO
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página: Transcrição de áudio em texto', 'redux-framework-demo' ),
            'id'               => 'TranscricaoaudioemTexto',
            'desc'             => __( 'Frases, textos e informações de cada sessão', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            )
        );

            //SESSÃO PÁGINA TRANSCRIÇÃO DE ÁUDIO EM TEXTO - ÁREA TEXTO QUEM SOMOS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área texto quem somos', 'redux-framework-demo' ),
                    'id'               => 'paginas_transcricao_quem_somos',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração quem somos', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_transcricao_quem_somos_video',
                            'type'     => 'text',
                            'title'    => __( 'Url do vídeo', 'redux-framework-demo' ),
                            'desc'    => __( 'Adicione o vídeo na área de mídias', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_transcricao_quem_somos_texto',
                            'type'     => 'editor',
                            'title'    => __( 'Texto quem somos', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_transcricao_quem_somos_btn',
                            'type'     => 'text',
                            'title'    => __( 'Título do botão', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_transcricao_quem_somos_btn_link',
                            'type'     => 'text',
                            'title'    => __( 'Link do botão', 'redux-framework-demo' ),
                            'desc'    => __( 'Ex:http://www.exemplo.com.br', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA TRANSCRIÇÃO - DESCRIÇÃO
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Texto  iformativo', 'redux-framework-demo' ),
                    'id'               => 'paginas_transcricao_iformativo',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração textos informativos', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'paginas_transcricao_informativo_texto',
                            'type'     => 'editor',
                            'title'    => __( 'texto informativo', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA TRANSCRIÇÃO - ÁREA CLIENTES
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área clientes', 'redux-framework-demo' ),
                    'id'               => 'paginas_transcricao_logo_clientes',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração área clientes', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'paginas_transcricao_logo_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título área clientes', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA TRANSCRIÇÃO - ÁREA VALORES TOPO
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Sessão de estatisticas topo', 'redux-framework-demo' ),
                    'id'               => 'paginas_transcricao_estatisticas',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração da sessão de estatísticas:', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'paginas_transcricao_valor1',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da primeiro sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_transcricao_texto_valor1',
                            'type'     => 'text',
                            'title'    => __( 'Texto da primeira sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_transcricao_valor2',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da segunda sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_transcricao_texto_valor2',
                            'type'     => 'text',
                            'title'    => __( 'Texto da segunda sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_transcricao_valor3',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da terceira sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_transcricao_texto_valor3',
                            'type'     => 'text',
                            'title'    => __( 'Texto da terceira sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_transcricao_valor4',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da quarta sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_transcricao_texto_valor4',
                            'type'     => 'text',
                            'title'    => __( 'Texto da quarta sessão:', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

        //SESSÃO PÁGINA DEGRAVAÇÃO DE ÁUDIO EM TEXTO
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página: Degravação de Áudio em Texto', 'redux-framework-demo' ),
            'id'               => 'DegravacaoAudioTexto',
            'desc'             => __( 'Frases, textos e informações de cada sessão', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            )
        );

            //SESSÃO PÁGINA DEGRAVAÇÃO DE ÁUDIO EM TEXTO - ÁREA TEXTO QUEM SOMOS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área texto quem somos', 'redux-framework-demo' ),
                    'id'               => 'paginas_degravacaoquem_somos',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração quem somos', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'opt_degravacaoquem_somos_video',
                            'type'     => 'text',
                            'title'    => __( 'Url do vídeo', 'redux-framework-demo' ),
                            'desc'    => __( 'Adicione o vídeo na área de mídias', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_degravacaoquem_somos_texto',
                            'type'     => 'editor',
                            'title'    => __( 'Texto quem somos', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'opt_degravacaoquem_somos_btn',
                            'type'     => 'text',
                            'title'    => __( 'Título do botão', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA DEGRAVAÇÃO DE ÁUDIO EM TEXTO - DESCRIÇÃO
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Texto  iformativo', 'redux-framework-demo' ),
                    'id'               => 'paginas_degravacaoiformativo',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração textos informativos', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'paginas_degravacaoinformativo_texto',
                            'type'     => 'editor',
                            'title'    => __( 'texto informativo', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA DEGRAVAÇÃO DE ÁUDIO EM TEXTO - ÁREA CLIENTES
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área  logo clientes', 'redux-framework-demo' ),
                    'id'               => 'paginas_degravacaologo_clientes',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração área clientes', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'paginas_degravacaologo_titulo',
                            'type'     => 'text',
                            'title'    => __( 'Título área clientes', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA DEGRAVAÇÃO - ÁREA ESTATISTICAS TOPO
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Sessão de estatisticas topo', 'redux-framework-demo' ),
                    'id'               => 'paginas_degravacao_estatisticas',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração da sessão de estatísticas:', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'paginas_degravacao_valor1',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da primeiro sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_degravacao_texto_valor1',
                            'type'     => 'text',
                            'title'    => __( 'Texto da primeira sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_degravacao_valor2',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da segunda sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_degravacao_texto_valor2',
                            'type'     => 'text',
                            'title'    => __( 'Texto da segunda sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_degravacao_valor3',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da terceira sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_degravacao_texto_valor3',
                            'type'     => 'text',
                            'title'    => __( 'Texto da terceira sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_degravacao_valor4',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da quarta sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_degravacao_texto_valor4',
                            'type'     => 'text',
                            'title'    => __( 'Texto da quarta sessão:', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

        //SESSÃO PÁGINA TRADUÇÃO JURAMENTADA
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página: Tradução Juramentada', 'redux-framework-demo' ),
            'id'               => 'traducaoJuramentada',
            'desc'             => __( 'Frases, textos e informações de cada sessão', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            )
        );

            //SESSÃO PÁGINA TRADUÇÃO JURAMENTADA - AREA TEXTO QUEM SOMOS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área texto quem somos', 'redux-framework-demo' ),
                    'id'               => 'paginas_traducaoJuramentada_texto_quem_somos',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração quem somos', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_traducaoJuramentada_url_video',
                            'type'  => 'text',
                            'title' => 'Url do vídeo',
                        ),
                        array(
                            'id'    => 'paginas_traducaoJuramentada_textoQuemSomos',
                            'type'  => 'editor',
                            'title' => 'Texto quem somos',
                        ),
                    )
                )
            );
            //SESSÃO PÁGINA TRADUCAO JURAMENTADA - ÁREA VALORES TOPO
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Sessão de estatisticas topo', 'redux-framework-demo' ),
                    'id'               => 'paginas_traducao_juramentada_estatisticas',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração da sessão de estatísticas:', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'       => 'paginas_traducao_juramentada_valor1',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da primeiro sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_traducao_juramentada_texto_valor1',
                            'type'     => 'text',
                            'title'    => __( 'Texto da primeira sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_traducao_juramentada_valor2',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da segunda sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_traducao_juramentada_texto_valor2',
                            'type'     => 'text',
                            'title'    => __( 'Texto da segunda sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_traducao_juramentada_valor3',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da terceira sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_traducao_juramentada_texto_valor3',
                            'type'     => 'text',
                            'title'    => __( 'Texto da terceira sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_traducao_juramentada_valor4',
                            'type'     => 'text',
                            'title'    => __( 'Valor numerico da quarta sessão:', 'redux-framework-demo' ),
                        ),
                        array(
                            'id'       => 'paginas_traducao_juramentada_texto_valor4',
                            'type'     => 'text',
                            'title'    => __( 'Texto da quarta sessão:', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

        //SESSÃO PÁGINA TRADUÇÃO SIMPLES
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página: Tradução Simples', 'redux-framework-demo' ),
            'id'               => 'traducaoSimples',
            'desc'             => __( 'Frases, textos e informações de cada sessão', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            )
        );
            //SESSÃO PÁGINA TRADUÇÃO SIMPLES - AREA TEXTO QUEM SOMOS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área texto quem somos', 'redux-framework-demo' ),
                    'id'               => 'paginas_traducaoSimples_texto_quem_somos',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração quem somos', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_traducaoSimples_url_video',
                            'type'  => 'text',
                            'title' => 'Url do vídeo',
                        ),
                        array(
                            'id'    => 'paginas_traducaoSimples_textoQuemSomos',
                            'type'  => 'editor',
                            'title' => 'Texto quem somos',
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA TRADUCAO SIMPLES - ÁREA VALORES TOPO
            Redux::setSection( $opt_name, array(
                        'title'            => __( 'Sessão de estatisticas topo', 'redux-framework-demo' ),
                        'id'               => 'paginas_traducaoSimples_estatisticas',
                        'subsection'       => true,
                        'desc'             => __( 'Configuração da sessão de estatísticas:', 'redux-framework-demo' ),
                        'customizer_width' => '450px',
                        'fields'           => array(
                            array(
                                'id'       => 'paginas_traducaoSimples_valor1',
                                'type'     => 'text',
                                'title'    => __( 'Valor numerico da primeiro sessão:', 'redux-framework-demo' ),
                            ),
                            array(
                                'id'       => 'paginas_traducaoSimples_texto_valor1',
                                'type'     => 'text',
                                'title'    => __( 'Texto da primeira sessão:', 'redux-framework-demo' ),
                            ),
                            array(
                                'id'       => 'paginas_traducaoSimples_valor2',
                                'type'     => 'text',
                                'title'    => __( 'Valor numerico da segunda sessão:', 'redux-framework-demo' ),
                            ),
                            array(
                                'id'       => 'paginas_traducaoSimples_texto_valor2',
                                'type'     => 'text',
                                'title'    => __( 'Texto da segunda sessão:', 'redux-framework-demo' ),
                            ),
                            array(
                                'id'       => 'paginas_traducaoSimples_valor3',
                                'type'     => 'text',
                                'title'    => __( 'Valor numerico da terceira sessão:', 'redux-framework-demo' ),
                            ),
                            array(
                                'id'       => 'paginas_traducaoSimples_texto_valor3',
                                'type'     => 'text',
                                'title'    => __( 'Texto da terceira sessão:', 'redux-framework-demo' ),
                            ),
                            array(
                                'id'       => 'paginas_traducaoSimples_valor4',
                                'type'     => 'text',
                                'title'    => __( 'Valor numerico da quarta sessão:', 'redux-framework-demo' ),
                            ),
                            array(
                                'id'       => 'paginas_traducaoSimples_texto_valor4',
                                'type'     => 'text',
                                'title'    => __( 'Texto da quarta sessão:', 'redux-framework-demo' ),
                            ),
                        )
                    )
                );

        //SESSÃO PÁGINA LEGENDAGEM PRODUÇÃO DE CONTEUDO
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página: Legendagem Produção de Conteúdo', 'redux-framework-demo' ),
            'id'               => 'legendagemProducaoConteudo',
            'desc'             => __( 'Frases, textos e informações de cada sessão', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            )
        );
            //SESSÃO PÁGINA PÁGINA LEGENDAGEM PRODUÇÃO DE CONTEUDO - AREA TEXTO QUEM SOMOS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área texto quem somos', 'redux-framework-demo' ),
                    'id'               => 'paginas_legendagem_producao_conteudo_texto_quem_somos',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração quem somos', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_legendagem_producao_conteudo_url_video',
                            'type'  => 'text',
                            'title' => 'Url do vídeo',
                        ),
                        array(
                            'id'    => 'paginas_legendagem_producao_conteudo_textoQuemSomos',
                            'type'  => 'editor',
                            'title' => 'Texto quem somos',
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA PÁGINA LEGENDAGEM PRODUÇÃO DE CONTEUDO - ÁREA VALORES TOPO
            Redux::setSection( $opt_name, array(
                            'title'            => __( 'Sessão de estatisticas topo', 'redux-framework-demo' ),
                            'id'               => 'paginas_legendagem_producao_conteudo_estatisticas',
                            'subsection'       => true,
                            'desc'             => __( 'Configuração da sessão de estatísticas:', 'redux-framework-demo' ),
                            'customizer_width' => '450px',
                            'fields'           => array(
                                array(
                                    'id'       => 'paginas_legendagem_producao_conteudo_valor1',
                                    'type'     => 'text',
                                    'title'    => __( 'Valor numerico da primeiro sessão:', 'redux-framework-demo' ),
                                ),
                                array(
                                    'id'       => 'paginas_legendagem_producao_conteudo_texto_valor1',
                                    'type'     => 'text',
                                    'title'    => __( 'Texto da primeira sessão:', 'redux-framework-demo' ),
                                ),
                                array(
                                    'id'       => 'paginas_legendagem_producao_conteudo_valor2',
                                    'type'     => 'text',
                                    'title'    => __( 'Valor numerico da segunda sessão:', 'redux-framework-demo' ),
                                ),
                                array(
                                    'id'       => 'paginas_legendagem_producao_conteudo_texto_valor2',
                                    'type'     => 'text',
                                    'title'    => __( 'Texto da segunda sessão:', 'redux-framework-demo' ),
                                ),
                                array(
                                    'id'       => 'paginas_legendagem_producao_conteudo_valor3',
                                    'type'     => 'text',
                                    'title'    => __( 'Valor numerico da terceira sessão:', 'redux-framework-demo' ),
                                ),
                                array(
                                    'id'       => 'paginas_legendagem_producao_conteudo_texto_valor3',
                                    'type'     => 'text',
                                    'title'    => __( 'Texto da terceira sessão:', 'redux-framework-demo' ),
                                ),
                                array(
                                    'id'       => 'paginas_legendagem_producao_conteudo_valor4',
                                    'type'     => 'text',
                                    'title'    => __( 'Valor numerico da quarta sessão:', 'redux-framework-demo' ),
                                ),
                                array(
                                    'id'       => 'paginas_legendagem_producao_conteudo_texto_valor4',
                                    'type'     => 'text',
                                    'title'    => __( 'Texto da quarta sessão:', 'redux-framework-demo' ),
                                ),
                            )
                        )
                    );

        //SESSÃO PÁGINA LEGENDAGEM EMPRESARIAL
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página: Legendagem Empresarial', 'redux-framework-demo' ),
            'id'               => 'legendagemEmpresarial',
            'desc'             => __( 'Frases, textos e informações de cada sessão', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            )
        );
            //SESSÃO PÁGINA PÁGINA LEGENDAGEM EMPRESARIAL - AREA TEXTO QUEM SOMOS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Área texto quem somos', 'redux-framework-demo' ),
                    'id'               => 'paginas_legendagem_empresarial_texto_quem_somos',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração quem somos', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_legendagem_empresarial_url_video',
                            'type'  => 'text',
                            'title' => 'Url do vídeo',
                        ),
                        array(
                            'id'    => 'paginas_legendagem_empresarial_textoQuemSomos',
                            'type'  => 'editor',
                            'title' => 'Texto quem somos',
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA PÁGINA LEGENDAGEM EMPRESARIAL - ÁREA VALORES TOPO
            Redux::setSection( $opt_name, array(
                                    'title'            => __( 'Sessão de estatisticas topo', 'redux-framework-demo' ),
                                    'id'               => 'paginas_legendagem_empresarial_estatisticas',
                                    'subsection'       => true,
                                    'desc'             => __( 'Configuração da sessão de estatísticas:', 'redux-framework-demo' ),
                                    'customizer_width' => '450px',
                                    'fields'           => array(
                                        array(
                                            'id'       => 'paginas_legendagem_empresarial_valor1',
                                            'type'     => 'text',
                                            'title'    => __( 'Valor numerico da primeiro sessão:', 'redux-framework-demo' ),
                                        ),
                                        array(
                                            'id'       => 'paginas_legendagem_empresarial_texto_valor1',
                                            'type'     => 'text',
                                            'title'    => __( 'Texto da primeira sessão:', 'redux-framework-demo' ),
                                        ),
                                        array(
                                            'id'       => 'paginas_legendagem_empresarial_valor2',
                                            'type'     => 'text',
                                            'title'    => __( 'Valor numerico da segunda sessão:', 'redux-framework-demo' ),
                                        ),
                                        array(
                                            'id'       => 'paginas_legendagem_empresarial_texto_valor2',
                                            'type'     => 'text',
                                            'title'    => __( 'Texto da segunda sessão:', 'redux-framework-demo' ),
                                        ),
                                        array(
                                            'id'       => 'paginas_legendagem_empresarial_valor3',
                                            'type'     => 'text',
                                            'title'    => __( 'Valor numerico da terceira sessão:', 'redux-framework-demo' ),
                                        ),
                                        array(
                                            'id'       => 'paginas_legendagem_empresarial_texto_valor3',
                                            'type'     => 'text',
                                            'title'    => __( 'Texto da terceira sessão:', 'redux-framework-demo' ),
                                        ),
                                        array(
                                            'id'       => 'paginas_legendagem_empresarial_valor4',
                                            'type'     => 'text',
                                            'title'    => __( 'Valor numerico da quarta sessão:', 'redux-framework-demo' ),
                                        ),
                                        array(
                                            'id'       => 'paginas_legendagem_empresarial_texto_valor4',
                                            'type'     => 'text',
                                            'title'    => __( 'Texto da quarta sessão:', 'redux-framework-demo' ),
                                        ),
                                    )
                                )
                            );

        //SESSÃO PÁGINA DE VAGAS
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página: Vagas', 'redux-framework-demo' ),
            'id'               => 'pagina-vagas',
            'desc'             => __( 'Configuração da página de vagas', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-website'
            )
        );
            //PRIMEIRA SESSÃO PÁGINA DE VAGAS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Primeira Sessão', 'redux-framework-demo' ),
                    'id'               => 'paginas_vagas_primeira_sessao',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração da sessão cresça com a Audiotext', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_vagas_titulo_sessao_cresca_com_Audio',
                            'type'  => 'text',
                            'title' => 'Título primeira sessão:',
                        ),
                        array(
                            'id'    => 'paginas_vagas_texto_sessao_cresca_com_Audio',
                            'type'  => 'editor',
                            'title' => 'Texto primeira sessão:',
                        ),
                        array(
                            'id'    => 'paginas_vagas_imagem_sessao_cresca_com_Audio',
                            'type'  => 'media',
                            'title' => 'Imagem primeira sessão:',
                        ),
                    )
                )
            );
            //SEGUNDA SESSÃO PÁGINA DE VAGAS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Segunda Sessão', 'redux-framework-demo' ),
                    'id'               => 'paginas_vagas_segunda_sessao',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração da sessão "Nosso time":', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_vagas_titulo_segunda_sessao',
                            'type'  => 'text',
                            'title' => 'Título segunda sessão:',
                        ),
                        array(
                            'id'    => 'paginas_vagas_texto_segunda_sessao',
                            'type'  => 'editor',
                            'title' => 'Texto segunda sessão:',
                        ),
                        array(
                            'id'    => 'paginas_vagas_imagem_segunda_sessao',
                            'type'  => 'media',
                            'title' => 'Imagem segunda sessão:',
                        ),
                    )
                )
            );
            //SESSÃO VAGAS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Sessão de Vagas', 'redux-framework-demo' ),
                    'id'               => 'paginas_vagas_sessao_vagas',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração da sessão de vagas:', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_vagas_titulo_sessao_vagas',
                            'type'  => 'text',
                            'title' => 'Título sessão de vagas:',
                        ),
                    )
                )
            );
            //PÁGINA DA VAGA
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'página da vaga', 'redux-framework-demo' ),
                    'id'               => 'paginas_vagas_vaga',
                    'subsection'       => true,
                    'desc'             => __( 'Configuração da página da vaga:', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_vagas_texto_botao',
                            'type'  => 'text',
                            'title' => 'Texto do botão para o formulário',
                        ),
                        array(
                            'id'    => 'paginas_vagas_logo_menu',
                            'type'  => 'media',
                            'title' => 'Logo do menu da página da vaga:',
                        ),
                    )
                )
            );
        //SESSÃO PÁGINA FAQ
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página: FAQ', 'redux-framework-demo' ),
            'id'               => 'faq',
            'desc'             => __( 'Frases, textos e informações de cada sessão', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-question'
            )
        );

            //SESSÃO PÁGINA FAQ - BANNER TOPO
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Banner topo', 'redux-framework-demo' ),
                    'id'               => 'paginas_faq_banner_topo',
                    'subsection'       => true,
                    'desc'             => __( 'Banner topo', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_faq_titulo',
                            'type'  => 'text',
                            'title' => 'Título banner',
                        ),
                        array(
                            'id'    => 'paginas_faq_descricao',
                            'type'  => 'text',
                            'title' => 'Descrição banner',
                        ),
                        array(
                            'id'    => 'paginas_faq_banner',
                            'type'  => 'media',
                            'desc'  => '1903 X 500',
                            'title' => 'Imagem banner',
                        ),
                    )
                )
            );

           //SESSÃO PÁGINA FAQ - ESCONDER E MOSTRAR ÁREAS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Esconder/Mostrar sessão', 'redux-framework-demo' ),
                    'id'               => 'paginas_faq_esconder_mostrar',
                    'subsection'       => true,
                    'desc'             => __( 'Esconder/Mostrar sessão', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_faq_um_texter_hidden',
                            'type'  => 'button_set',
                            'desc'  => 'Selecione se deseja mostrar essa área',
                            'title' => 'Área seja um texter',
                            'options' => array(
                                'Esconder' => 'Esconder',
                                'Mostrar' => 'Mostrar',
                            ),
                        ),
                    )
                )
            );

        //SESSÃO PÁGINA CONTATO
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página: Contato', 'redux-framework-demo' ),
            'id'               => 'contato',
            'desc'             => __( 'Frases, textos e informações de cada sessão', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-iphone-home'
            )
        );
            //SESSÃO PÁGINA CONTATO
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Informações de texto/Formulário', 'redux-framework-demo' ),
                    'id'               => 'pagina_contato',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_contato_titulo',
                            'type'  => 'text',
                            'title' => 'Título',
                        ),
                        array(
                            'id'    => 'paginas_contato_descricao',
                            'type'  => 'textarea',
                            'title' => 'Descrição',
                        ),
                        array(
                            'id'    => 'paginas_contato_formulario',
                            'type'  => 'text',
                            'desc'    => __( 'Crie um formulário no menu de contato', 'redux-framework-demo' ),
                            'title' => 'Shortcode formulário',
                        ),
                    )
                )
            );

           //SESSÃO PÁGINA CONTATO - ESCONDER E MOSTRAR ÁREAS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Esconder/Mostrar sessão', 'redux-framework-demo' ),
                    'id'               => 'paginas_faq_esconder_mostrar',
                    'subsection'       => true,
                    'desc'             => __( 'Esconder/Mostrar sessão', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_faq_um_texter_hidden',
                            'type'  => 'button_set',
                            'desc'  => 'Selecione se deseja mostrar essa área',
                            'title' => 'Área seja um texter',
                            'options' => array(
                                'Esconder' => 'Esconder',
                                'Mostrar' => 'Mostrar',
                            ),
                        ),
                    )
                )
            );

        //SESSÃO PÁGINA QUEM SOMOS
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página: Quem Somos', 'redux-framework-demo' ),
            'id'               => 'quemSOmos',
            'desc'             => __( 'Frases, textos e informações de cada sessão', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-user'
            )
        );
            //SESSÃO PÁGINA QUEM SOMOS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Informações de texto/Formulário', 'redux-framework-demo' ),
                    'id'               => 'pagina_quemSomos',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_quemSomos_titulo',
                            'type'  => 'text',
                            'title' => 'Título',
                        ),
                        array(
                            'id'    => 'paginas_quemSomos_descricao',
                            'type'  => 'textarea',
                            'title' => 'Descrição',
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA QUEM SOMOS - ESCONDER E MOSTRAR ÁREAS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Esconder/Mostrar sessão', 'redux-framework-demo' ),
                    'id'               => 'paginas_faq_esconder_mostrar',
                    'subsection'       => true,
                    'desc'             => __( 'Esconder/Mostrar sessão', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_faq_um_texter_hidden',
                            'type'  => 'button_set',
                            'desc'  => 'Selecione se deseja mostrar essa área',
                            'title' => 'Área seja um texter',
                            'options' => array(
                                'Esconder' => 'Esconder',
                                'Mostrar' => 'Mostrar',
                            ),
                        ),
                    )
                )
            );

        //SESSÃO PÁGINA AGÊNCIAS
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página: Agências', 'redux-framework-demo' ),
            'id'               => 'agencias',
            'desc'             => __( 'Frases, textos e informações de cada sessão', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-group-alt'
            )
        );
            //SESSÃO PÁGINA AGÊNCIAS - BANNER TOPO
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Banner topo', 'redux-framework-demo' ),
                    'id'               => 'paginas_agencias_banner_topo',
                    'subsection'       => true,
                    'desc'             => __( 'Banner topo', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_agencias_titulo',
                            'type'  => 'text',
                            'title' => 'Título banner',
                        ),
                        array(
                            'id'    => 'paginas_agencias_banner',
                            'type'  => 'media',
                            'desc'  => '1903 X 500',
                            'title' => 'Imagem banner',
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA AGÊNCIAS - FORMULÁRIO
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Formulário', 'redux-framework-demo' ),
                    'id'               => 'paginas_agencias_formulario',
                    'subsection'       => true,
                    'desc'             => __( '', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_agencias_formulario_shortcode',
                            'type'  => 'text',
                            'title'    => __( 'Shortcode formulário', 'redux-framework-demo' ),
                            'desc'    => __( 'Crie um formulário no menu de contato', 'redux-framework-demo' ),
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA QUEM SOMOS - ESCONDER E MOSTRAR ÁREAS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Esconder/Mostrar sessão', 'redux-framework-demo' ),
                    'id'               => 'paginas_agencia_esconder_mostrar',
                    'subsection'       => true,
                    'desc'             => __( 'Esconder/Mostrar sessão', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_agencia_carrossel_hidden',
                            'type'  => 'button_set',
                            'desc'  => 'Selecione se deseja mostrar essa área',
                            'title' => 'Área carrossel de agências',
                            'options' => array(
                                'Esconder' => 'Esconder',
                                'Mostrar' => 'Mostrar',
                            ),
                        ),
                        array(
                            'id'    => 'paginas_agencia_servicos_hidden',
                            'type'  => 'button_set',
                            'desc'  => 'Selecione se deseja mostrar essa área',
                            'title' => 'Área serviços',
                            'options' => array(
                                'Esconder' => 'Esconder',
                                'Mostrar' => 'Mostrar',
                            ),
                        ),
                        array(
                            'id'    => 'paginas_agencia_um_texter_hidden',
                            'type'  => 'button_set',
                            'desc'  => 'Selecione se deseja mostrar essa área',
                            'title' => 'Área seja um texter',
                            'options' => array(
                                'Esconder' => 'Esconder',
                                'Mostrar' => 'Mostrar',
                            ),
                        ),
                    )
                )
            );

        //SESSÃO PÁGINA ENTRE PARA O TIME
        Redux::setSection( $opt_name, array(
            'title'            => __( 'Página: Entre para o time', 'redux-framework-demo' ),
            'id'               => 'entreParaTime',
            'desc'             => __( 'Frases, textos e informações de cada sessão', 'redux-framework-demo' ),
            'customizer_width' => '400px',
            'icon'             => 'el el-group'
            )
        );
            //SESSÃO PÁGINA ENTRE PARA O TIME - BANNER TOPO
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Banner topo', 'redux-framework-demo' ),
                    'id'               => 'paginas_entreParaTime_banner_topo',
                    'subsection'       => true,
                    'desc'             => __( 'Banner topo', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_entreParaTime_titulo',
                            'type'  => 'text',
                            'title' => 'Título banner',
                        ),
                        array(
                            'id'    => 'paginas_entreParaTime_descricao',
                            'type'  => 'text',
                            'title' => 'Descrição banner',
                        ),
                        array(
                            'id'    => 'paginas_entreParaTime_banner',
                            'type'  => 'media',
                            'desc'  => '1903 X 500',
                            'title' => 'Imagem banner',
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA ENTRE PARA O TIME  - CONTEÚDO
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Conteúdo', 'redux-framework-demo' ),
                    'id'               => 'paginas_entreParaTime_conteudo',
                    'subsection'       => true,
                    'desc'             => __( 'Conteúdo', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_entreParaTime_conteudo_descricao',
                            'type'  => 'editor',
                            'title' => 'Descrição',
                        ),
                        array(
                            'id'    => 'paginas_entreParaTime_conteudo_img',
                            'type'  => 'media',
                            'title' => 'Imagem ilustrativa',
                        ),
                        array(
                            'id'    => 'paginas_entreParaTime_conteudo_desc',
                            'type'  => 'text',
                            'title' => 'Informações',
                        ),
                        array(
                            'id'    => 'paginas_entreParaTime_formulario',
                            'type'  => 'text',
                            'title' => 'Formulário',
                        ),
                         array(
                            'id'    => 'paginas_entreParaTime_form_hidden',
                            'type'  => 'button_set',
                            'desc'  => 'Selecione se deseja mostrar o formulário',
                            'title' => 'Mostrar formulário',
                            'options' => array(
                                'Esconder' => 'Esconder',
                                'Mostrar' => 'Mostrar',
                            ),
                        ),
                    )
                )
            );

            //SESSÃO PÁGINA ENTRE PARA O TIMe - ESCONDER E MOSTRAR ÁREAS
            Redux::setSection( $opt_name, array(
                    'title'            => __( 'Esconder/Mostrar sessão', 'redux-framework-demo' ),
                    'id'               => 'paginas_entreParaTime_esconder_mostrar',
                    'subsection'       => true,
                    'desc'             => __( 'Esconder/Mostrar sessão', 'redux-framework-demo' ),
                    'customizer_width' => '450px',
                    'fields'           => array(
                        array(
                            'id'    => 'paginas_entreParaTime_servicos_hidden',
                            'type'  => 'button_set',
                            'desc'  => 'Selecione se deseja mostrar essa área',
                            'title' => 'Área serviços',
                            'options' => array(
                                'Esconder' => 'Esconder',
                                'Mostrar' => 'Mostrar',
                            ),
                        ),
                        array(
                            'id'    => 'paginas_entreParaTime_um_texter_hidden',
                            'type'  => 'button_set',
                            'desc'  => 'Selecione se deseja mostrar essa área',
                            'title' => 'Área seja um texter',
                            'options' => array(
                                'Esconder' => 'Esconder',
                                'Mostrar' => 'Mostrar',
                            ),
                        ),
                    )
                )
            );
        //SESSÃO INFORMAÇÕES DE CONTATO
        Redux::setSection( $opt_name, array(
                'title'            => __( 'Informações de contato/Endereço', 'redux-framework-demo' ),
                'id'               => 'informacoes_contato',
                'desc'             => __( '', 'redux-framework-demo' ),
                'customizer_width' => '450px',
                'icon' => 'el el-phone-alt',
                'fields'           => array(
                    array(
                        'id'    => 'info_endereco',
                        'type'  => 'multi_text',
                        'title' => 'Telefones',
                        'desc' => 'Brasília:DF: (61) 3329-6037',
                        'placeholder' => 'Whatsapp:Whatsapp: (41) 99799-0622',
                    ),
                  array(
                        'id'    => 'info_emial',
                        'type'  => 'multi_text',
                        'title' => 'E-mail',
                        'desc' => 'RH: texter@audiotext.com.br',
                    ),
                    array(
                        'id'    => 'info_horario',
                        'type'  => 'text',
                        'title' => 'Horário de Atendimento',
                        'desc' => 'Segunda à sexta das 9h às 18h',
                    ),
                    array(
                        'id'    => 'info_face',
                        'type'  => 'text',
                        'title' => 'Link facebook',
                    ),
                    array(
                        'id'    => 'info_youtube',
                        'type'  => 'text',
                        'title' => 'Link youtube',
                    ),
                )
            )
        );

        //SESSÃO SCIPTS TOPO
        Redux::setSection( $opt_name, array(
                'title'            => __( 'Scripts topo', 'redux-framework-demo' ),
                'id'               => 'scripts_topo',
                'desc'             => __( '', 'redux-framework-demo' ),
                'customizer_width' => '450px',
                'icon' => 'el el-wrench',
                'fields'           => array(
                    array(
                        'id'    => 'scripts_topo_head',
                        'type'  => 'textarea',
                        'title' => 'Scripts head',
                        'desc' => ' ',
                        'placeholder' => '
                            <!-- ADICIONE DESTA FORMA O NOME DO SCRIPT 1  -->
                            <script>
                               var hkn = { code: "Audiotext" };
                               (function () {
                                   var h = document.createElement( "script"); h.type = "text/javascript"; h.async = true;
                                   h.src = ("https:" == document.location.protocol ? "https://" : "http://") + "tag.hariken.co/hkn.js";
                                   var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(h, s);
                               })();
                            </script>
                            <!-- ADICIONE DESTA FORMA O NOME DO SCRIPT 2  -->
                            <script>
                               var hkn = { code: "Audiotext" };
                               (function () {
                                   var h = document.createElement( "script"); h.type = "text/javascript"; h.async = true;
                                   h.src = ("https:" == document.location.protocol ? "https://" : "http://") + "tag.hariken.co/hkn.js";
                                   var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(h, s);
                               })();
                            </script>
                        ',
                    ),
                    array(
                        'id'    => 'scripts_topo_body',
                        'type'  => 'textarea',
                        'title' => 'Scripts body',
                        'desc' => '',
                        'placeholder' => '
                            <!-- ADICIONE DESTA FORMA O NOME DO SCRIPT 1  -->
                            <script>
                               var hkn = { code: "Audiotext" };
                               (function () {
                                   var h = document.createElement( "script"); h.type = "text/javascript"; h.async = true;
                                   h.src = ("https:" == document.location.protocol ? "https://" : "http://") + "tag.hariken.co/hkn.js";
                                   var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(h, s);
                               })();
                            </script>
                            <!-- ADICIONE DESTA FORMA O NOME DO SCRIPT 2  -->
                            <script>
                               var hkn = { code: "Audiotext" };
                               (function () {
                                   var h = document.createElement( "script"); h.type = "text/javascript"; h.async = true;
                                   h.src = ("https:" == document.location.protocol ? "https://" : "http://") + "tag.hariken.co/hkn.js";
                                   var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(h, s);
                               })();
                            </script>
                        ',
                    ),
                )
            )
        );

        //SESSÃO SCIPTS FOOTER
        Redux::setSection( $opt_name, array(
                'title'            => __( 'Scripts rodapé', 'redux-framework-demo' ),
                'id'               => 'scripts_footer',
                'desc'             => __( '', 'redux-framework-demo' ),
                'customizer_width' => '450px',
                'icon' => 'el el-wrench',
                'fields'           => array(
                    array(
                        'id'    => 'scripts_footer_campo',
                        'type'  => 'textarea',
                        'title' => 'Scripts',
                        'desc' => '',
                        'placeholder' => '
                            <!-- ADICIONE DESTA FORMA O NOME DO SCRIPT 1  -->
                            <script>
                               var hkn = { code: "Audiotext" };
                               (function () {
                                   var h = document.createElement( "script"); h.type = "text/javascript"; h.async = true;
                                   h.src = ("https:" == document.location.protocol ? "https://" : "http://") + "tag.hariken.co/hkn.js";
                                   var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(h, s);
                               })();
                            </script>
                            <!-- ADICIONE DESTA FORMA O NOME DO SCRIPT 2  -->
                            <script>
                               var hkn = { code: "Audiotext" };
                               (function () {
                                   var h = document.createElement( "script"); h.type = "text/javascript"; h.async = true;
                                   h.src = ("https:" == document.location.protocol ? "https://" : "http://") + "tag.hariken.co/hkn.js";
                                   var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(h, s);
                               })();
                            </script>
                        ',
                    ),
                )
            )
        );

        //SESSÃO GERAL
        Redux::setSection( $opt_name, array(
                'title'            => __( 'Sessão Geral', 'redux-framework-demo' ),
                'id'               => 'sessao_geral',
                'desc'             => __( '', 'redux-framework-demo' ),
                'customizer_width' => '450px',
                'icon' => 'el el-wrench',
                'fields'           => array(
                  array(
                      'id'    => 'botao_carrosselEsquerda',
                      'type'  => 'media',
                      'title' => 'Botão esquerdo dos carrosseis:',
                      'desc' => 'Insira aqui o botão esquerdo dos carrosseis.',
                  ),
                  array(
                      'id'    => 'botao_carrosselDireita',
                      'type'  => 'media',
                      'title' => 'Botão direito dos carrosseis:',
                      'desc' => 'Insira aqui o botão direito dos carrosseis.',
                  ),
                  array(
                      'id'    => 'opt_imagem_onde_nao_tem_video',
                      'type'  => 'media',
                      'title' => 'Imagem para as páginas onde não tem vídeo.',
                  ),
                )
            )
        );

    /*********************************************
                 //FUNÇÕES//
    **********************************************/
        Redux::setSection( $opt_name, array(
            'icon'            => 'el el-list-alt',
            'title'           => __( 'Customizer Only', 'redux-framework-demo' ),
            'desc'            => __( '<p class="description">This Section should be visible only in Customizer</p>', 'redux-framework-demo' ),
            'customizer_only' => true,
            'fields'          => array(
                array(
                    'id'              => 'opt-customizer-only',
                    'type'            => 'select',
                    'title'           => __( 'Customizer Only Option', 'redux-framework-demo' ),
                    'subtitle'        => __( 'The subtitle is NOT visible in customizer', 'redux-framework-demo' ),
                    'desc'            => __( 'The field desc is NOT visible in customizer.', 'redux-framework-demo' ),
                    'customizer_only' => true,
                    //Must provide key => value pairs for select options
                    'options'         => array(
                        '1' => 'Opt 1',
                        '2' => 'Opt 2',
                        '3' => 'Opt 3'
                    ),
                    'default'         => '2'
                ),
            )
        ) );
        /*
         * <--- END SECTIONS
         */
        /*
         *
         * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
         *
         */
        /*
        *
        * --> Action hook examples
        *
        */
        // If Redux is running as a plugin, this will remove the demo notice and links
        //add_action( 'redux/loaded', 'remove_demo' );
        // Function to test the compiler hook and demo CSS output.
        // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
        //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);
        // Change the arguments after they've been declared, but before the panel is created
        //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );
        // Change the default value of a field after it's been set, but before it's been useds
        //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );
        // Dynamically add a section. Can be also used to modify sections/fields
        //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');
        /**
         * This is a test function that will let you see when the compiler hook occurs.
         * It only runs if a field    set with compiler=>true is changed.
         * */
        if ( ! function_exists( 'compiler_action' ) ) {
            function compiler_action( $options, $css, $changed_values ) {
                echo '<h1>The compiler hook has run!</h1>';
                echo "<pre>";
                print_r( $changed_values ); // Values that have changed since the last save
                echo "</pre>";
                //print_r($options); //Option values
                //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
            }
        }
        /**
         * Custom function for the callback validation referenced above
         * */
        if ( ! function_exists( 'redux_validate_callback_function' ) ) {
            function redux_validate_callback_function( $field, $value, $existing_value ) {
                $error   = false;
                $warning = false;
                //do your validation
                if ( $value == 1 ) {
                    $error = true;
                    $value = $existing_value;
                } elseif ( $value == 2 ) {
                    $warning = true;
                    $value   = $existing_value;
                }
                $return['value'] = $value;

                if ( $error == true ) {
                    $return['error'] = $field;
                    $field['msg']    = 'your custom error message';
                }

                if ( $warning == true ) {
                    $return['warning'] = $field;
                    $field['msg']      = 'your custom warning message';
                }
                return $return;
            }
        }
        /**
         * Custom function for the callback referenced above
         */
        if ( ! function_exists( 'redux_my_custom_field' ) ) {
            function redux_my_custom_field( $field, $value ) {
                print_r( $field );
                echo '<br/>';
                print_r( $value );
            }
        }
        /**
         * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
         * Simply include this function in the child themes functions.php file.
         * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
         * so you must use get_template_directory_uri() if you want to use any of the built in icons
         * */
        if ( ! function_exists( 'dynamic_section' ) ) {
            function dynamic_section( $sections ) {
                //$sections = array();
                $sections[] = array(
                    'title'  => __( 'Section via hook', 'redux-framework-demo' ),
                    'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
                    'icon'   => 'el el-paper-clip',
                    // Leave this as a blank section, no options just some intro text set above.
                    'fields' => array()
                );
                return $sections;
            }
        }
        /**
         * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
         * */
        if ( ! function_exists( 'change_arguments' ) ) {
            function change_arguments( $args ) {
                //$args['dev_mode'] = true;
                return $args;
            }
        }
        /**
         * Filter hook for filtering the default value of any given field. Very useful in development mode.
         * */
        if ( ! function_exists( 'change_defaults' ) ) {
            function change_defaults( $defaults ) {
                $defaults['str_replace'] = 'Testing filter hook!';
                return $defaults;
            }
        }
        /**
         * Removes the demo link and the notice of integrated demo from the redux-framework plugin
         */
        if ( ! function_exists( 'remove_demo' ) ) {
            function remove_demo() {
                // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
                if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                    remove_filter( 'plugin_row_meta', array(
                        ReduxFrameworkPlugin::instance(),
                        'plugin_metalinks'
                    ), null, 2 );
                    // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                    remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
                }
            }
        }
