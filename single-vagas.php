<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Audiotext
 */
$category_detail=get_the_category($post->ID);
$cargoVaga = rwmb_meta('Audiotext_vaga_cargo');
$cidadeVaga = rwmb_meta('Audiotext_vaga_cidade');
$tempoDaVaga = rwmb_meta('Audiotext_vaga_tempo');
get_header(); ?>
    <style>header {display: none!important;} @media (max-width: 767px){  .pg { padding-top: 175px !important;}}</style>

	<div class="pg pg-vaga">
        <div class="menuVaga">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="logoVaga">
                            <a href="<?php echo get_home_url(); ?>">
                                <img src="<?php echo $configuracao['paginas_vagas_logo_menu']['url'] ?>" alt="Logo AudioText">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="infoVagaMenu">
                            <span class="nomeDaVaga"><?php echo get_the_title(); ?></span>
                            <a href="#" class="abrirModalEntreParaOTime"><?php echo $configuracao['paginas_vagas_texto_botao'] ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-8">   
                    <div class="informacoesDaVaga">
                        <h2 class="tituloVaga"><?php echo get_the_title(); ?></h2>
                        <h3 class="localVaga"><?php echo $cidadeVaga ?></h3>
                        <span class="tempoDaVaga"><?php echo $tempoDaVaga ?></span>
                    </div>
                    <div class="conteudoDaVaga">
		                <?php echo the_content(); ?>
           		 	</div>
                </div>    
                <div class="col-md-4">
                    <div class="botaoVaga">
                        <a href="#" class="abrirModalEntreParaOTime"><?php echo $configuracao['paginas_vagas_texto_botao'] ?></a>
                    </div>
                </div>
            </div>
        </div>
	</div>
<?php

get_footer();
