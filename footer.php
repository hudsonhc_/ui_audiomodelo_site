<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Audiotext
 */
global $configuracao;
?>
	<div class="geral" style="display: none;" id="formOrcamento">
		<style>.geral ul.gform_fields {
			display: block;
		    width: 100%;
		    max-width: 600px;
		    margin: 0 auto!important;
		}
		</style>
		<div class="dimensoes">
			<div class="formularioOrcamentoEsquerda">
				<?php echo do_shortcode('[gravityform id="3" title="true" description="true" ajax="true"]'); ?>
			</div>
		</div>
		<span class="fechar" id="fecharOrcamento"><i class="far fa-times-circle"></i></span>
	</div>

	<div class="formularioEntreParaOTime" style="display: none;">
		<div class="camposEntreParaOTime">
			<?php echo do_shortcode('[gravityform id="4" title="true" description="true" ajax="true"]'); ?>
		</div>
		<span class="fecharEntreParaOTime">
			<i class="far fa-times-circle"></i>
		</span>
	</div>


<footer class="ropdape">

	<div class="container containerFull">

		<div class="row">

			<div class="col-sm-3">

				<div class="contatoTelefones">

					<strong><i class="fas fa-phone"></i> TELEFONE</strong>

					<ul>

					<?php

						// FOREACH PARA PEGAR OS NUMEROS DE CONTATO

						$i = 0;

						if ($configuracao['info_endereco']):

							$info_enderecoRoape = $configuracao['info_endereco'];

							foreach ($info_enderecoRoape as $info_enderecoRoape):

								$telefonesContato = $info_enderecoRoape;

								$telefoneContato = explode(":", $telefonesContato);

								if ($telefoneContato[0] == "Whatsapp"):

					?>

								<li><a href="tel:<?php echo $telefoneContato[2] ?>" title="<?php echo $telefoneContato[0] ?>"><?php echo $telefoneContato[0] ?> : <?php echo $telefoneContato[2] ?> </a></li>

									<?php else: ?>

								<li><a href="tel:<?php echo $telefoneContato[2] ?>" title="<?php echo $telefoneContato[0] ?>"><?php echo $telefoneContato[0] ?> : <?php echo $telefoneContato[2] ?></a></li>

					<?php  endif;$i++;endforeach;endif;?>

					</ul>

				</div>

			</div>



			<div class="col-sm-3">

				<div class="contatoEmails">

					<strong><i class="fas fa-at"></i> EMAIL</strong>

					<ul>

					<?php

						// FOREACH PARA PEGAR OS E-MAILS DE CONTATO

						$i = 0;

						if ($configuracao['info_emial']):

						$info_emialRodape = $configuracao['info_emial'];

						foreach ($info_emialRodape as $info_emialRodape):

							$emailContatoRodape = explode(":", $info_emialRodape);



					?>

					<li><a href="malito:<?php echo $emailContatoRodape[1]  ?>" title="<?php echo $emailContatoRodape[1]  ?>">  <?php echo $emailContatoRodape[1]  ?></a><li>

					<?php $i++; endforeach;endif;?>

					</ul>

				</div>

			</div>



			<div class="col-sm-3">

				<div class="endereco">

					<strong> ENDEREÇO</strong>

					<ul>

						<?php

							//LOOP DE POST ENDEREÇOS

							$postEnderecos = new WP_Query( array( 'post_type' => 'endereco', 'orderby' => 'id', 'posts_per_page' => -1) );

							while ( $postEnderecos->have_posts() ) : $postEnderecos->the_post();

							$Audiotext_endereco_endereco = rwmb_meta('Audiotext_endereco_endereco');



						?>

						<li>

							<a href="https://www.google.com.br/maps/place/<?php echo $Audiotext_endereco_endereco ?>"  itemprop="street-address"target="_blank">

								<b><?php echo get_the_title(); ?></b>

								<?php echo $Audiotext_endereco_endereco = rwmb_meta('Audiotext_endereco_endereco'); ?>

							</a>

						</li>

					<?php endwhile; wp_reset_query(); ?>

					</ul>

				</div>

			</div>



			<div class="col-sm-3">

				<nav>

					<span>páginas do site</span>

					<?php

						$menuAudioText = array(

							'theme_location'  => '',

							'menu'            => 'Menu Rodapé AudioText',

							'container'       => false,

							'container_class' => '',

							'container_id'    => '',

							'menu_class'      => '',

							'menu_id'         => '',

							'echo'            => true,

							'fallback_cb'     => 'wp_page_menu',

							'before'          => '',

							'after'           => '',

							'link_before'     => '',

							'link_after'      => '',

							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',

							'depth'           => 2,

							'walker'          => ''

							);

						wp_nav_menu( $menuAudioText );

					?>

				</nav>

			</div>

		</div>



	</div>

</footer>

<div class="copyright">

	<div class="container">

		<div class="row">

			<div class="col-sm-6">

				<p class="direitoreservados">© Audiotext 2018. Todos os direitos reservados.</p>

			</div>

			<div class="col-sm-6 ">

				<div class="redesSociais">

				<a href="<?php echo $configuracao['info_face'] ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>

					<a href="<?php echo $configuracao['info_youtube'] ?>" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a>

				</div>

			</div>



		</div>

	</div>



</div>

<?php echo $configuracao['scripts_footer'] ?>

<?php wp_footer(); ?>





</body>

</html>
