<?php
/**
* Template Name: Vagas
* Description:
*
* @package audiotext
*/
global $post;
global $configuracao;
get_header(); ?>
	<div class="texters">
		<section class="sobre">
			<div class="row">
				<div class="col-sm-6">
					<div class="textoSobre">
						<h2><?php echo $configuracao['paginas_vagas_titulo_sessao_cresca_com_Audio']; ?></h2>
						<p><?php echo $configuracao['paginas_vagas_texto_sessao_cresca_com_Audio']; ?><p>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="imagem right">
						<figure>
							<img src="<?php echo $configuracao['paginas_vagas_imagem_sessao_cresca_com_Audio']['url'] ?>" alt="Imagem">
						</figure>
					</div>
				</div>
			</div>
		</section>
		<section class="sobre">
			<div class="row">
				<div class="col-sm-6">
					<div class="imagem right">
						<figure>
							<img src="<?php echo $configuracao['paginas_vagas_imagem_segunda_sessao']['url'] ?>" alt="Imagem">
						</figure>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="textoSobre">
						<h2><?php echo $configuracao['paginas_vagas_titulo_segunda_sessao'] ?></h2>
						<p><?php echo $configuracao['paginas_vagas_texto_segunda_sessao'] ?><p>
					</div>
				</div>
			</div>
		</section>
		<div class="container">
			<section class="vagasDisponveis">
				<h2><?php echo $configuracao['paginas_vagas_titulo_sessao_vagas'] ?></h2>
					<?php 
						// DEFINE A TAXONOMIA
						$taxonomia = 'categoriavagas';

						// LISTA AS CATEGORIAS 
						$categoriavagas = get_terms( $taxonomia, array(
							'orderby'    => 'description',
							'order' => 'DSC',
							'hide_empty' => 0,
							'parent'	 => 0
						));
						$i = 0;
				?>
                <div class="opcoesVagas">
					<div id="carrosselDeVagas" class="owl-Carousel">
						<?php  
							foreach ($categoriavagas as $categoriavagas):
								if ($i == 0):
								 $ativo = "categoriaAtiva";
						?>
                        <div class="item ">
                            <button data-id="<?php echo $categoriavagas->term_id ?>" class="<?php echo $ativo ?>">
                                <h2><?php echo $categoriavagas->name ?></h2>
                            </button>
                        </div>
                        <?php else: ?>
                	 	<div class="item ">
                            <button data-id="<?php echo $categoriavagas->term_id ?>">
                                <h2><?php echo $categoriavagas->name ?></h2>
                            </button>
                        </div>
                        <?php endif; $i++;endforeach;?>
                    </div>
				</div>
				<?php 
					// LISTA AS CATEGORIAS 
					$categoriatab = get_terms( $taxonomia, array(
						'orderby'    => 'description',
						'order' => 'DSC',
						'hide_empty' => 0,
						'parent'	 => 0
					));
					$j = 0;
				
					foreach ($categoriatab as $categoriatab):

						if ($j == 0):
						 $ativo = "categoriaAtiva";


				?>
					<div class="areaDescricaoVagas <?php echo $ativo ?>" id="<?php echo $categoriatab->term_id ?>">
						<div class="vagas">
							<?php 
								//LOOP DE POST CATEGORIA DESTAQUE				
								$vagas = new WP_Query(array(
									'post_type'     => 'vagas',
									'posts_per_page'   => -1,
									'tax_query'     => array(
										array(
											'taxonomy' => 'categoriavagas',
											'field'    => 'slug',
											'terms'    => $categoriatab->slug,
											)
										)
									)
								);
								// LOOP DE POST
								while ( $vagas->have_posts() ) : $vagas->the_post();
							?>
							<a href="<?php echo get_permalink() ?>">
								<h2><?php echo get_the_title() ?></h2>
								<span class="setor"><?php echo $setor = rwmb_meta('Audiotext_vaga_cargo'); ?></span>
								<span class="localDaVaga"><?php echo $local = rwmb_meta('Audiotext_vaga_cidade'); ?></span>
							</a>
							<?php  endwhile; wp_reset_query();  ?>
						</div>
					</div>
				<?php else: ?>
					<div class="areaDescricaoVagas" id="<?php echo $categoriatab->term_id ?>">
						<div class="vagas">
							<?php 
								//LOOP DE POST CATEGORIA DESTAQUE				
								$vagas = new WP_Query(array(
									'post_type'     => 'vagas',
									'posts_per_page'   => -1,
									'tax_query'     => array(
										array(
											'taxonomy' => 'categoriavagas',
											'field'    => 'slug',
											'terms'    => $categoriatab->slug,
											)
										)
									)
								);
								// LOOP DE POST
								while ( $vagas->have_posts() ) : $vagas->the_post();
							?>
							<a href="<?php echo get_permalink() ?>">
								<h2><?php echo get_the_title() ?></h2>
								<span class="setor"><?php echo $setor = rwmb_meta('Audiotext_vaga_cargo'); ?></span>
								<span class="localDaVaga"><?php echo $local = rwmb_meta('Audiotext_vaga_cidade'); ?></span>
							</a>
							<?php  endwhile; wp_reset_query();  ?>
						</div>
					</div>

				 <?php endif; $j++; endforeach;?>
			</section>
		</div>
	</div>
<?php 

get_footer(); ?>