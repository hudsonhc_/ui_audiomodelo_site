$(function(){

	$(".item button").click(function(){
	 	let idVaga = $(this).attr("data-id");
	 	$( ".vagasDisponveis .areaDescricaoVagas").hide();
	 	$( ".vagasDisponveis " + "#"+idVaga).show();
	});
    
    //CARROSSEL DE DESTAQUE
	$("#carrosselDeVagas").owlCarousel({
		items : 5,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,
        margin: 0,

	    //CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
         responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },

            991:{
                items:3
            },
            1024:{
                items:5
            },
            1440:{
                items:5
},
            			            
         }		    		   		    
	    
	});
			
});